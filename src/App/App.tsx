import React, {Component, Suspense} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {NavBar, Spinner} from "../Components";
import lazyImport from "../Helpers/DelayedImport";

const Home = lazyImport(() => import('../Pages/Home'));
const FFXIV = lazyImport(() => import('../Pages/FFXIV'));

export class App extends Component {
    public render(): JSX.Element {
        return (
            <Router>
                <div className="h-100 w-100 d-flex flex-column">
                    <NavBar/>
                    <div className="flex-grow-1">
                        <Suspense fallback={<Spinner/>}>
                            <Switch>
                                <Route exact path="/" component={Home}/>
                                <Route exact path="/ffxiv" component={FFXIV}/>
                            </Switch>
                        </Suspense>
                    </div>
                    <div style={{textAlign: "center"}}>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
                </div>
            </Router>
        );
    }
}
import React from "react";

const Spinner = () => {
    return (
        <div className="row justify-content-center align-items-center h-100 w-100">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    );
}

export default Spinner;
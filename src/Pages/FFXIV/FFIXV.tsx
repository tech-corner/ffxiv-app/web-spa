import React, {Component} from "react";

export default class FFXIV extends Component {
    public render() {
        return (
            <div className="row justify-content-center align-items-center w-100 h-100">
                <div>
                    <h3 className="text-center">
                        FFXIV Crafting helper
                    </h3>
                    <p className="text-center">
                        When implemented there will be some UI component.
                    </p>
                </div>
            </div>
        );
    }
}